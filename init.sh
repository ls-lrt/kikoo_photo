#!/bin/bash

# inline function helpers
root_dir() { echo "$1" | sed 's/[^/]*$//'; }

dir=${1:-..}
script_abs_path=$(root_dir $(readlink -f ${0}))
index_html="${script_abs_path}index.html"
player_js="${script_abs_path}player.js"
player_css="${script_abs_path}style.css"
player_dir=$(basename ${script_abs_path})
player_html=""
css_html=""

if [ ! -f "$index_html" ]; then
    echo "$index_html not found, generating it..."
    echo "Please give the url where the player will be located, eg:
http://example.com/music/kikoo_player or leave it empty to put it in
index.html file."
    read -p "player location: " player_location
    if [ "X"$player_location = "X" ]; then
	player_html="<script>"`cat $player_js`"</script>"
    else
	player_html="<script type=\"text/javascript\" src=\"${player_location}\"></script>"
    fi
    css_html="<style>"`cat $player_css`"</style>"
    cat <<EOF > $index_html
<html>
  <title>.:Photo browser:.</title>
  $css_html
  <body>
    <div id="galery"></div>
    <div id="myModal" class="modal">
    <span class="prev" title="précédent">&vartriangleleft;</span><span class="next" title="suivant">&vartriangleright;</span><span class="close" title="fermer">&times;</span>
    <a id="a_pic"> <img class="modal-content" id="pic"> </a>
    <div id="caption"></div>
    </div>
  </body>
  $player_html
</html>
EOF
fi

walk()
{
    cd "$1"
    echo -n "."
    if [ $(basename $(pwd)) != "$player_dir" ]; then
	if [ -d "thumbs" -a -e "playlist.m3u" ]; then
	    find -maxdepth 1 -iname "*.jpg" | sort > "playlist.m3u.new"
	    diff playlist.m3u playlist.m3u.new | grep -E '^>' | awk '{print $2}' > "diff.m3u"
	    if [[ -s "diff.m3u" ]]; then
		mogrify -path thumbs -thumbnail 300x200 `cat diff.m3u`
		chmod o+r `cat diff.m3u`
	    fi
	    rm -f "diff.m3u"
	    mv "playlist.m3u.new" "playlist.m3u"
	fi
	if [ `find -type d -not -path '*/\.*' | wc -l` -eq 1 ]; then
	    if [ ! -d thumbs ]; then
		mkdir thumbs
		find -maxdepth 1 -iname "*.jpg" | sort > "playlist.m3u"
		mogrify -path thumbs -thumbnail 300x200 `cat playlist.m3u`
		chmod o+r `cat playlist.m3u`
		rm -f index.html && ln -s "$index_html" .
	    fi
	else
	    for i in *; do
		if [ -d "$i" -a "$i" != "thumbs" ]; then
		    walk "$i"
		fi
	    done
	fi
    fi
    cd ..
}

echo -n "Initializing"
walk "$dir"
echo "done!"
