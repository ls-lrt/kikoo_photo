window.onload = function() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = process;
    xhr.open("GET", "playlist.m3u", true);
    xhr.send();

    //handling image display
    var is_showing_image = false;
    var modal = document.getElementById('myModal');
    var modal_img = document.getElementById("pic");
    var modal_img_a = document.getElementById("a_pic");
    var modal_txt = document.getElementById("caption");
    var span_close_modal = document.getElementsByClassName("close")[0];
    var span_next_modal = document.getElementsByClassName("next")[0];
    var span_prev_modal = document.getElementsByClassName("prev")[0];

    //action buttons
    span_close_modal.onclick = modal_close;
    span_next_modal.onclick = modal_next;
    span_prev_modal.onclick = modal_prev;
    
    //handling miniatures
    var tracks;
    var galery = document.getElementById("galery");
    var last_miniature_displayed = 0;
    var miniature_width_px = 300;
    var miniature_height_px = 200;
    var n_by_line = Math.floor(window.innerWidth / miniature_width_px) || 1 ;
    var n_by_column = Math.floor(window.innerHeight / miniature_height_px) || 1;

    function process() {
	if (xhr.readyState == 4) {
	    tracks = xhr.responseText.match(/[^\r\n]+/g);
	    init_miniature();
	    display_n(n_by_line * n_by_column);
	}
    }

    var last_scrollY = 0;
    window.addEventListener('scroll', function(e) {
	if (!is_showing_image) {
	    if (window.scrollY > ((last_miniature_displayed + 2) / n_by_line) * miniature_height_px && window.scrollY > last_scrollY) {
		console.log("need to display more!");
		display_n_more(n_by_line);
		last_scrollY = window.scrollY;
	    }
	}
    });

    function modal_close() {
	modal.style.display='none';
	is_showing_image = false;
    }

    function modal_next() {   
	if (modal_img.alt !== "") {
	    var index = (Number(modal_img.alt) + 1) % tracks.length;
	    modal_img.src = tracks[index];
	    modal_img.alt = String(index);
	    modal_img_a.href= tracks[index];
	}
    }

    function modal_prev() {   
	if (modal_img.alt !== "") {
	    var index = (Number(modal_img.alt) - 1);
	    index = (index < 0) ? tracks.length + index : index;
	    modal_img.src = tracks[index];
	    modal_img.alt = String(index);
	    modal_img_a.href= tracks[index];
	}
    }

    function init_miniature() {
	for (var t in tracks) {
	    galery.innerHTML += '<div class="responsive"> <div class="img"> <img id="' + tracks[t] + '" alt="' + t + '" width="' + miniature_width_px + '" height="' + miniature_height_px + '"> </div> </div>';
	}
	for (var t in tracks) {
	    var img = document.getElementById(tracks[t]);
	    img.onclick = function(){
		modal.style.display = "block";
		modal_img.src = this.id;
		modal_img.alt = this.alt;
		modal_img_a.href= this.id;
		is_showing_image = true;
		//modal_txt.innerHTML = ;
	    }
	}
    }

    function display_n(n) {
	for (var i = last_miniature_displayed; i < tracks.length && i < n; i++) {
	    var img = document.getElementById(tracks[i]);
	    img.src = "thumbs/" + tracks[i];
	    last_miniature_displayed = i;
	}
    }

    function display_n_more(n) {
	display_n(last_miniature_displayed + n);
    }
    
    document.addEventListener('keydown', function(ev) {
	var to;
	var modal_img = document.getElementById("pic");
	switch (ev.keyCode) {
	case 27: //escape
	    modal_close();
	    break;
	case 37: // <-
	    modal_prev();
	    break;
	case 39: // ->
	    modal_next();
	    break;
	default:
	    modal_close();
	    break;
	}
    });
};
